Source: r-cran-rstan
Maintainer: Debian R Packages Maintainers <r-pkg-team@alioth-lists.debian.net>
Uploaders: Andreas Tille <tille@debian.org>
Section: gnu-r
Testsuite: autopkgtest-pkg-r
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-r,
               r-base-dev,
               r-cran-stanheaders (>= 2.32.0),
               r-cran-inline (>= 0.3.19),
               r-cran-gridextra,
               r-cran-rcpp (>= 1.0.7),
               r-cran-rcppparallel (>= 5.1.4),
               r-cran-loo,
               r-cran-pkgbuild,
               r-cran-quickjsr,
               r-cran-ggplot2 (>= 3.3.5),
               r-cran-rcppeigen (>= 0.3.4.0.0),
               r-cran-bh (>= 1.75.0-0),
               r-cran-v8
Standards-Version: 4.7.2
Vcs-Browser: https://salsa.debian.org/r-pkg-team/r-cran-rstan
Vcs-Git: https://salsa.debian.org/r-pkg-team/r-cran-rstan.git
Homepage: https://cran.r-project.org/package=rstan
Rules-Requires-Root: no

Package: r-cran-rstan
Architecture: any
Depends: ${R:Depends},
         ${shlibs:Depends},
         ${misc:Depends}
Recommends: ${R:Recommends}
Suggests: ${R:Suggests}
Description: GNU R interface to Stan
 User-facing R functions are provided to parse, compile, test, estimate,
 and analyze Stan models by accessing the header-only Stan library
 provided by the 'StanHeaders' package. The Stan project develops a
 probabilistic programming language that implements full Bayesian
 statistical inference via Markov Chain Monte Carlo, rough Bayesian
 inference via 'variational' approximation, and (optionally penalized)
 maximum likelihood estimation via optimization. In all three cases,
 automatic differentiation is used to quickly and accurately evaluate
 gradients without burdening the user with the need to derive the partial
 derivatives.
